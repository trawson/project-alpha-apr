from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


@login_required
def projects(request):
    items = Project.objects.filter(owner=request.user)
    context = {"projects": items}
    return render(request, "projects/list.html", context)


@login_required
def project_details(request, id):
    items = get_object_or_404(Project, id=id)
    context = {"project_details": items}
    return render(request, "projects/details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("/projects/")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
